package com.tekton.labs.service;

import com.tekton.labs.dto.ProductDetailDto;
import com.tekton.labs.dto.ProductDto;
import com.tekton.labs.mapper.ProductDetailMapper;
import com.tekton.labs.mapper.ProductMapper;
import com.tekton.labs.mapper.ResponsibleMapper;
import com.tekton.labs.model.Product;
import com.tekton.labs.repository.ProductDetailRepository;
import com.tekton.labs.repository.ProductRepository;
import com.tekton.labs.repository.ResponsibleRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class ProductServiceTest {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ResponsibleRepository responsibleRepository;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private ProductDetailMapper productDetailMapper;

    @Autowired
    private ResponsibleMapper responsibleMapper;

    @Autowired
    private ProductDetailRepository productDetailRepository;

    private ProductDetailService productDetailService;

    private ResponsibleService responsibleService;

    private ProductService productService;

    private List<Product> products;

    @Mock
    private RetoolService retoolService;

    @BeforeEach
    void init() {
        Product product = new Product(1L, "Luxury BMW", "Car", "BMW");
        Product product2 = new Product(2L, "Luxury Toyota", "Car", "Toyota");
        Product product3 = new Product(3L, "Mercedez", "Truck", "Mercedez Benz");
        products = productRepository.saveAll(List.of(product, product2, product3));
        productDetailService = new ProductDetailService(productDetailRepository, productDetailMapper);
        responsibleService = new ResponsibleService(responsibleRepository, responsibleMapper);
        productService = new ProductService(this.productRepository, this.productMapper, productDetailService, responsibleService, retoolService);
    }

    @AfterEach
    void clean() {
        productDetailRepository.deleteAll();
        productRepository.deleteAll();
    }

    @Test
    void getById() throws Exception {
        ProductDto productDto = productService.getById(products.get(0).getId());
        assertEquals("BMW", productDto.getBrand());
        assertEquals("Car", productDto.getType());
    }

    @Test
    void insert(){
        ProductDto productDto = ProductDto.builder()
                .name("Honda")
                .type("Car")
                .brand("Honda")
                .details(
                    List.of(ProductDetailDto.builder()
                            .expiredYear(2020)
                            .fabricationYear(2019)
                            .build())
                )
                .build();
        productDto = productService.insert(productDto);
        assertEquals("Honda", productDto.getBrand());
        assertEquals("Car", productDto.getType());
        assertNotNull(productDto.getId());
    }

    @Test
    void update(){
        ProductDto productDto = ProductDto.builder()
                .id(products.get(2).getId())
                .name("2021 Mercedez Benz")
                .type("Car")
                .brand("Toyota")
                .details(
                    List.of(ProductDetailDto.builder()
                            .expiredYear(2020)
                            .fabricationYear(2019)
                            .build())
                )
                .build();
        productDto = productService.update(productDto);
        assertEquals("2021 Mercedez Benz", productDto.getName());
        assertEquals("Car", productDto.getType());
    }


}
