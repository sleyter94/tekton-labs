package com.tekton.labs.repository;

import com.tekton.labs.model.Product;
import com.tekton.labs.service.ProductService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest
public class ProductRepositoryTest {

    @Autowired
    private ProductRepository productRepository;

    private List<Product> products;

    @BeforeEach
    void init(){
        Product product = new Product(null, "Luxury BMW", "Car", "BMW");
        Product product2 = new Product(null, "Luxury Toyota", "Car", "Toyota");
        Product product3 = new Product(null, "Mercedez", "Truck", "Mercedez Benz");
        products = productRepository.saveAll(List.of(product, product2, product3));
    }

    @AfterEach
    void clean(){
        productRepository.deleteAll();
    }

    @Test
    void getProduct() throws Exception {
        var product = productRepository.findById(products.get(2).getId()).orElseThrow(() -> new Exception("Product Not Found"));
        assertEquals("Mercedez", product.getName());
        assertEquals("Truck", product.getType());
    }

    @Test
    void insertProduct(){
        var product = new Product(null, "Nissan Yaris", "Car", "Nissan");
        product = productRepository.save(product);
        assertNotNull(product.getId());
        assertEquals("Nissan", product.getBrand());
    }

    @Test
    void updateProduct(){
        var product = new Product(products.get(1).getId(), "Nissan Yaris", "Car", "Nissan");
        product = productRepository.save(product);
        assertEquals(products.get(1).getId(), product.getId());
        assertEquals("Nissan", product.getBrand());
    }
}
