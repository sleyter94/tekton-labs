package com.tekton.labs.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tekton.labs.dto.ProductDetailDto;
import com.tekton.labs.dto.ProductDto;
import com.tekton.labs.service.ProductService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest
public class ProductControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private ProductService productService;

    @Test
    void getById() throws Exception {
        Long productId = 1L;
        ProductDto productDto = ProductDto.builder()
                .id(productId)
                .name("BMW")
                .type("Car")
                .brand("BMW")
                .build();
        when(productService.getById(productId)).thenReturn(productDto);
        mockMvc.perform(MockMvcRequestBuilders.get("/products/"+productId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", is("BMW")))
                .andExpect(jsonPath("$.id", is(productId.intValue())));
    }


    @Test
    void insert() throws Exception {
        Long productId = 1L;
        ProductDto productDto = ProductDto.builder()
                .name("Luxury BMW")
                .type("Car")
                .brand("BMW")
                .details(
                    List.of(ProductDetailDto.builder()
                            .expiredYear(2020)
                            .fabricationYear(2019)
                            .build())
                )
                .build();
        ProductDto productSavedDto = ProductDto.builder()
                .id(productId)
                .name("Luxury BMW")
                .type("Car")
                .brand("BMW")
                .details(
                    List.of(ProductDetailDto.builder()
                            .id(1L)
                            .expiredYear(2020)
                            .fabricationYear(2019)
                            .build()))
                .build();
        when(productService.insert(productDto)).thenReturn(productSavedDto);
        ObjectMapper mapper = new ObjectMapper();
        mockMvc.perform(MockMvcRequestBuilders.post("/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(productDto)))
                .andExpect(status().isCreated());
    }

    @Test
    void update() throws Exception {
        Long productId = 1L;
        ProductDto productDto = ProductDto.builder()
                .id(productId)
                .name("Luxury BMW")
                .type("Car")
                .brand("BMW")
                .build();
        when(productService.update(productDto)).thenReturn(productDto);
        ObjectMapper mapper = new ObjectMapper();
        mockMvc.perform(MockMvcRequestBuilders.put("/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(productDto)))
                .andExpect(status().isNoContent());
    }
}
