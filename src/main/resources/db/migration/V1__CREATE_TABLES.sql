create table IF not exists tbl_product (
    id int not null,
    name varchar(50),
    type varchar(50),
    brand varchar(50),
    primary key(id)
);

create table IF not exists tbl_product_detail (
    id int not null,
    fabrication_year int,
    expired_year int,
    product_id int not null,
    primary key(id)
);
alter table tbl_product_detail add constraint FKixj658kn3qxri7q9msqtix9sf foreign key (product_id) references tbl_product;
create sequence seq_product start with 1 increment by 1;
create sequence seq_product_detail start with 1 increment by 1;