create table IF not exists tbl_responsible (
    id int not null,
    name varchar(50),
    email varchar(50),
    primary key(id)
);

insert into tbl_responsible(id,name, email)
values (1, 'Sleyter Sandoval', 'sleyter94@gmail.com')