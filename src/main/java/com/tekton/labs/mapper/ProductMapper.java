package com.tekton.labs.mapper;

import com.tekton.labs.dto.ProductDto;
import com.tekton.labs.model.Product;
import org.mapstruct.Mapper;

@Mapper
public interface ProductMapper {

    ProductDto toDto(Product product);
    Product toEntity(ProductDto productDto);
}
