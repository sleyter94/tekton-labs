package com.tekton.labs.mapper;

import com.tekton.labs.dto.ProductDetailDto;
import com.tekton.labs.model.ProductDetail;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface ProductDetailMapper {

    List<ProductDetailDto> toDto(List<ProductDetail> productDetail);
    ProductDetailDto toDto(ProductDetail productDetail);
    ProductDetail toEntity(ProductDetailDto productDetailDto);
    List<ProductDetail> toEntity(List<ProductDetailDto> productDetailDto);
}
