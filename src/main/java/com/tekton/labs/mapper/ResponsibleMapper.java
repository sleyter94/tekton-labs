package com.tekton.labs.mapper;

import com.tekton.labs.dto.ResponsibleDto;
import com.tekton.labs.model.Responsible;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper
public interface ResponsibleMapper {

    List<ResponsibleDto> toDto(List<Responsible> responsibles);
}
