package com.tekton.labs.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ProductDetailDto {
    private Long id;
    private Integer fabricationYear;
    private Integer expiredYear;
}
