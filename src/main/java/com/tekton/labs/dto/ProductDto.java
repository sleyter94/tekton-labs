package com.tekton.labs.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@Builder
public class ProductDto {

    private Long id;

    @NotNull(message = "Debe especificar el nombre")
    @NotEmpty(message = "Nombre ingresado no valido")
    private String name;

    @NotNull(message = "Debe especificar el tipo")
    @NotEmpty(message = "Tipo ingresado no valido")
    private String type;

    @NotNull(message = "Debe especificar la marca")
    @NotEmpty(message = "Marca ingresada no valida")
    private String brand;

    private String developerEmail;
    private String developerName;

    private String remote;

    @NotNull(message = "Debe especificar el detalle del producto")
    List<ProductDetailDto> details;

}
