package com.tekton.labs.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name="tbl_responsible")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Responsible implements Serializable {

    @Id
    private Integer id;

    private String name;
    private String email;
}
