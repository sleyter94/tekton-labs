package com.tekton.labs.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name="tbl_product")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Product implements Serializable {

    @Id
    @SequenceGenerator(name="seq_product", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "seq_product")
    private Long id;
    private String name;
    private String type;
    private String brand;

}
