package com.tekton.labs.controller;

import com.tekton.labs.dto.ProductDto;
import com.tekton.labs.performance.LogExecutionTime;
import com.tekton.labs.service.IProductService;
import io.micrometer.core.annotation.Timed;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/products")
public class ProductController {

    private IProductService productService;

    public ProductController(IProductService productoService) {
        this.productService = productoService;
    }

    @GetMapping("/{productId}")
    @LogExecutionTime
    public ProductDto getById(@PathVariable Long productId) throws Exception {
        return productService.getById(productId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @LogExecutionTime
    public void insert(@Valid @RequestBody ProductDto productDto) {
        productService.insert(productDto);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @LogExecutionTime
    public void update(@RequestBody ProductDto productDto) {
        productService.update(productDto);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @LogExecutionTime
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

}
