package com.tekton.labs.repository;

import com.tekton.labs.model.ProductDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

public interface ProductDetailRepository extends JpaRepository<ProductDetail, Long> {

    @Modifying
    @Transactional
    @Query("DELETE FROM ProductDetail pd WHERE pd.product.id = ?1")
    void deleteByProductId(Long productId);

    @Query("SELECT pd FROM ProductDetail pd WHERE pd.product.id = ?1")
    List<ProductDetail> findByProduct(Long productId);
}
