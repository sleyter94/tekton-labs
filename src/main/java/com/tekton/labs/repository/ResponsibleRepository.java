package com.tekton.labs.repository;

import com.tekton.labs.model.Responsible;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ResponsibleRepository extends JpaRepository<Responsible, Integer> {
}
