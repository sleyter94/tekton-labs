package com.tekton.labs.service;

import com.tekton.labs.dto.ResponsibleDto;

import java.util.List;

public interface IResponsibleService {

    List<ResponsibleDto> list();
}
