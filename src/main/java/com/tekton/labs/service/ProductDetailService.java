package com.tekton.labs.service;

import com.tekton.labs.dto.ProductDetailDto;
import com.tekton.labs.dto.ProductDto;
import com.tekton.labs.mapper.ProductDetailMapper;
import com.tekton.labs.model.Product;
import com.tekton.labs.model.ProductDetail;
import com.tekton.labs.repository.ProductDetailRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ProductDetailService implements IProductDetailService {

    private ProductDetailRepository productDetailRepository;
    private ProductDetailMapper productDetailMapper;

    public ProductDetailService(ProductDetailRepository productDetailRepository, ProductDetailMapper productDetailMapper) {
        this.productDetailRepository = productDetailRepository;
        this.productDetailMapper = productDetailMapper;
    }

    @Override
    public List<ProductDetailDto> listByProduct(Long productId) {
        return productDetailMapper.toDto(productDetailRepository.findByProduct(productId));
    }

    @Override
    public List<ProductDetailDto> insertByProduct(List<ProductDetailDto> productDetailDtos, Long productId) {
        List<ProductDetail> productDetails = productDetailMapper.toEntity(productDetailDtos);
        productDetails.forEach(productDetail -> {
            Product product = new Product();
            product.setId(productId);
            productDetail.setProduct(product);
        });
        return productDetailMapper.toDto(productDetailRepository.saveAll(productDetails));
    }

    @Override
    @Transactional
    public List<ProductDetailDto> updateByProduct(List<ProductDetailDto> productDetailDtos, Long productId) {
        List<ProductDetail> productDetails = productDetailMapper.toEntity(productDetailDtos);
        productDetails.forEach(productDetail -> {
            Product product = new Product();
            product.setId(productId);
            productDetail.setProduct(product);
        });
        return productDetailMapper.toDto(productDetailRepository.saveAll(productDetails));
    }

    @Override
    @Transactional
    public void deleteByProduct(ProductDto productDto) {
        productDetailRepository.deleteByProductId(productDto.getId());
    }
}
