package com.tekton.labs.service;

import com.tekton.labs.dto.ResponsibleDto;
import com.tekton.labs.mapper.ResponsibleMapper;
import com.tekton.labs.repository.ResponsibleRepository;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ResponsibleService implements IResponsibleService {

    private ResponsibleRepository responsibleRepository;
    private ResponsibleMapper responsibleMapper;

    public ResponsibleService(ResponsibleRepository responsibleRepository, ResponsibleMapper responsibleMapper) {
        this.responsibleRepository = responsibleRepository;
        this.responsibleMapper = responsibleMapper;
    }

    @Override
    @Cacheable("responsibles")
    public List<ResponsibleDto> list() {
        return responsibleMapper.toDto(responsibleRepository.findAll());
    }
}
