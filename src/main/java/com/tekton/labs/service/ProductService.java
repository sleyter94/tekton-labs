package com.tekton.labs.service;

import com.tekton.labs.dto.ProductDto;
import com.tekton.labs.dto.ResponsibleDto;
import com.tekton.labs.mapper.ProductMapper;
import com.tekton.labs.model.Product;
import com.tekton.labs.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Map;

@Service
@Slf4j
public class ProductService implements IProductService{

    private ProductRepository productRepository;
    private ProductMapper productMapper;
    private IProductDetailService productDetailService;
    private IResponsibleService responsibleService;
    private RetoolService retoolService;

    public ProductService(ProductRepository productRepository, ProductMapper productMapper,
                          @Qualifier("productDetailService") IProductDetailService productDetailService, IResponsibleService responsibleService, RetoolService retoolService) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
        this.productDetailService = productDetailService;
        this.responsibleService = responsibleService;
        this.retoolService = retoolService;
    }

    @Override
    public ProductDto getById(Long productId) throws Exception {

        return productRepository
            .findById(productId)
            .map(productMapper::toDto)
            .map(productDto -> {
                ResponsibleDto responsibleDto = responsibleService.list().get(0);
                productDto.setDeveloperEmail(responsibleDto.getEmail());
                productDto.setDeveloperName(responsibleDto.getName());
                log.info("Getting data from cache");
                return productDto;
            })
            .map(productDto -> {
                Map<String, String> data = retoolService.getRemote(productId.intValue());
                productDto.setRemote(data.get("remote"));
                log.info("Getting data from external service");
                return productDto;
            })
            .map(productDto -> {
                productDto.setDetails(productDetailService.listByProduct(productId));
                log.info("Getting details of products");
                return productDto;
            })
            .orElseThrow(() -> new Exception("Product Not Found"));
    }

    @Override
    @Transactional
    public ProductDto insert(ProductDto productDto) {
        Product product = productRepository.save(productMapper.toEntity(productDto));
        productDto.setId(product.getId());
        productDto.setDetails(productDetailService.insertByProduct(productDto.getDetails(), product.getId()));
        return productDto;
    }

    @Override
    @Transactional
    public ProductDto update(ProductDto productDto) {
        Product product = productRepository.save(productMapper.toEntity(productDto));
        productDetailService.deleteByProduct(productDto);
        productDto.setDetails(productDetailService.updateByProduct(productDto.getDetails(), product.getId()));
        return productDto;
    }
}
