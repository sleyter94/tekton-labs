package com.tekton.labs.service;

import com.tekton.labs.dto.ProductDetailDto;
import com.tekton.labs.dto.ProductDto;

import java.util.List;

public interface IProductDetailService {

    List<ProductDetailDto> listByProduct(Long productId);

    List<ProductDetailDto> insertByProduct(List<ProductDetailDto> productDetailDtos, Long productId);

    List<ProductDetailDto> updateByProduct(List<ProductDetailDto> productDetailDtos, Long productId);

    void deleteByProduct(ProductDto productDto);

}
