package com.tekton.labs.service;

import com.tekton.labs.dto.ProductDto;

public interface IProductService {

    ProductDto getById(Long productId) throws Exception;

    ProductDto insert(ProductDto productDto);

    ProductDto update(ProductDto productDto);
}
