package com.tekton.labs.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Map;

@FeignClient(name="retool",url="${retool.url}")
public interface RetoolService {

    @GetMapping("/{id}")
    Map<String, String> getRemote(@PathVariable Integer id);
}
